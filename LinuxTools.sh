#!/bin/bash
version="v3.1"
version_log="Fedora 系统支持"

RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
PLAIN="\033[0m"

red(){
    echo -e "\033[31m\033[01m$1\033[0m"
}

green(){
    echo -e "\033[32m\033[01m$1\033[0m"
}

yellow(){
    echo -e "\033[33m\033[01m$1\033[0m"
}


# 多方式判断操作系统，如非支持的操作系统，则退出脚本
REGEX=("debian" "ubuntu|jammy" "centos|red hat|kernel|oracle linux|alma|rocky" "'amazon linux'" "fedora" "alpine")
RELEASE=("Debian" "Ubuntu" "CentOS" "CentOS" "Fedora" "Alpine" "Ubuntu")

PACKAGE_UPDATE=("apt-get update" "apt-get update" "yum -y update" "yum -y update" "yum -y update" "apk update -f")
PACKAGE_INSTALL=("apt -y install" "apt -y install" "yum -y install" "yum -y install" "yum -y install" "apk add -f")
PACKAGE_UNINSTALL=("apt -y autoremove" "apt -y autoremove" "yum -y autoremove" "yum -y autoremove" "yum -y autoremove" "apk del -f")

[[ $EUID -ne 0 ]] && red "注意：请在root用户下运行脚本" && exit 1

CMD=("$(grep -i pretty_name /etc/os-release 2>/dev/null | cut -d \" -f2)" "$(hostnamectl 2>/dev/null | grep -i system | cut -d : -f2)" "$(lsb_release -sd 2>/dev/null)" "$(grep -i description /etc/lsb-release 2>/dev/null | cut -d \" -f2)" "$(grep . /etc/redhat-release 2>/dev/null)" "$(grep . /etc/issue 2>/dev/null | cut -d \\ -f1 | sed '/^[ ]*$/d')")

for i in "${CMD[@]}"; do
    SYS="$i" && [[ -n $SYS ]] && break
done

for ((int = 0; int < ${#REGEX[@]}; int++)); do
    if [[ $(echo "$SYS" | tr '[:upper:]' '[:lower:]') =~ ${REGEX[int]} ]]; then
        SYSTEM="${RELEASE[int]}" && [[ -n $SYSTEM ]] && break
    fi
done

[[ -z $SYSTEM ]] && red "不支持的VPS系统, 请使用主流的操作系统"




# 某些系统未自带 curl，检测并安装
if [[ -z $(type -P curl) ]]; then
    if [[ ! $SYSTEM == "CentOS" ]]; then
        ${PACKAGE_UPDATE[int]}
    fi
    ${PACKAGE_INSTALL[int]} curl
fi




# 检查系统内核版本
main=$(uname -r | awk -F . '{print $1}')
minor=$(uname -r | awk -F . '{print $2}')
# 获取系统版本号
OSID=$(grep -i version_id /etc/os-release | cut -d \" -f2 | cut -d . -f1)
# 检查VPS虚拟化
VIRT=$(systemd-detect-virt)


# 检测 VPS 处理器架构
archAffix() {
    case "$(uname -m)" in
        i386 | i686) echo '386' ;;
        x86_64 | amd64) echo 'amd64' ;;
        armv8 | arm64 | aarch64) echo 'arm64' ;;
        s390x) echo 's390x' ;;
        *) red "不支持的CPU架构!" && exit 1 ;;
    esac
}





open_ports(){
    systemctl stop firewalld.service 2>/dev/null
    systemctl disable firewalld.service 2>/dev/null
    setenforce 0 2>/dev/null
    ufw disable 2>/dev/null
    iptables -P INPUT ACCEPT 2>/dev/null
    iptables -P FORWARD ACCEPT 2>/dev/null
    iptables -P OUTPUT ACCEPT 2>/dev/null
    iptables -t nat -F 2>/dev/null
    iptables -t mangle -F 2>/dev/null
    iptables -F 2>/dev/null
    iptables -X 2>/dev/null
    netfilter-persistent save 2>/dev/null
    green "VPS的防火墙端口已放行！"
    echo ""  
    # echo -e "${YELLOW}1秒进入主菜单${YELLOW}"
    # echo "" 
    # sleep 1s 
    # menu
    
    read -rp " 任意键回主菜单" menuInput
    case $menuInput in
            *)clear && menu ;;
    esac

}



Root_sh(){

    echo ""    
    
    echo ""

    IP=$(curl -sm8 ip.sb)

    sudo lsattr /etc/passwd /etc/shadow >/dev/null 2>&1
    sudo chattr -i /etc/passwd /etc/shadow >/dev/null 2>&1
    sudo chattr -a /etc/passwd /etc/shadow >/dev/null 2>&1
    sudo lsattr /etc/passwd /etc/shadow >/dev/null 2>&1

    read -p "输入要设置的SSH端口（默认22）：" sshport
    [[ -z $sshport ]] && red "端口未设置，将使用默认22端口" && sshport=22
    read -p "输入设置的root密码：" password
    [[ -z $password ]] && red "密码未设置，将使用随机生成密码" && password=$(cat /proc/sys/kernel/random/uuid)
    echo root:$password | sudo chpasswd root

    sudo sed -i "s/^#\?Port.*/Port $sshport/g" /etc/ssh/sshd_config;
    sudo sed -i "s/^#\?PermitRootLogin.*/PermitRootLogin yes/g" /etc/ssh/sshd_config;
    sudo sed -i "s/^#\?PasswordAuthentication.*/PasswordAuthentication yes/g" /etc/ssh/sshd_config;

    sudo service ssh restart >/dev/null 2>&1 # 某些VPS系统的ssh服务名称为ssh，以防无法重启服务导致无法立刻使用密码登录
    sudo service sshd restart >/dev/null 2>&1

    yellow "VPS root登录信息设置完成！"
    green "VPS登录地址：$IP:$sshport"
    green "用户名：root"
    green "密码：$password"
    yellow "请妥善保存好登录信息！然后重启VPS确保设置已保存！"
    echo ""  
    
    read -rp " 任意键回主菜单" menuInput
    case $menuInput in
            *)clear && menu ;;
    esac
}


v6_dns64(){
    wg-quick down wgcf 2>/dev/null
    v66=`curl -s6m8 https://ip.gs -k`
    v44=`curl -s4m8 https://ip.gs -k`
    if [[ -z $v44 && -n $v66 ]]; then
        echo -e "nameserver 2a01:4f8:c2c:123f::1" > /etc/resolv.conf
        green "设置DNS64服务器成功！"
    else
        red "非纯IPv6 VPS，设置DNS64服务器失败！"
    fi
    wg-quick up wgcf 2>/dev/null
    
    read -rp " 任意键回主菜单" menuInput
    case $menuInput in
            *)clear && menu ;;
    esac
}




ddns_go(){
        
    # 使用命令获取最新版本号并保存到变量
    latest_version=$(wget -qO- -t1 -T2 "https://github.com/jeessy2/ddns-go/releases/latest" | grep "tag_name" | head -n 1 | awk -F ":" '{print $2}' | sed 's/\"//g;s/,//g;s/ //g')

    # 打印版本号
    echo -e "最新版本号为：$latest_version"

    # 下载文件

    curl -L https://gitlab.com/yuban/Rootlinux/-/raw/main/ddns-go_5.6.0_linux_$(archAffix).tar.gz?inline=false --output ddns-go_5.6.0_linux_$(archAffix).tar.gz

    mkdir ddns-go

    # 解压文件
    # tar -xzf ddns-go_5.6.0_linux_$(archAffix).tar.gz ddns-go
    tar -xzf ddns-go_5.6.0_linux_arm64.tar.gz -C ddns-go

    # 删除原目录
    rm -rf ddns-go_5.6.0_linux_$(archAffix).tar.gz

    chmod +x ddns-go/ddns-go
    ddns-go/ddns-go -s install

}






Huan_yuan(){
    
    echo -e ""
    mv /etc/apt/sources.list /etc/apt/sources.list.bak > /dev/null 2>&1
    mv /etc/apt/sources.list.d/armbian.list /etc/apt/sources.list.d/armbian.list.bak > /dev/null 2>&1
    echo "
    deb http://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free
    deb-src http://mirrors.tuna.tsinghua.edu.cn/debian/ buster main contrib non-free
    deb http://mirrors.tuna.tsinghua.edu.cn/debian/ buster-updates main contrib non-free
    deb-src http://mirrors.tuna.tsinghua.edu.cn/debian/ buster-updates main contrib non-free
    deb http://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free
    deb-src http://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free
    deb http://mirrors.tuna.tsinghua.edu.cn/debian-security buster/updates main contrib non-free
    deb-src http://mirrors.tuna.tsinghua.edu.cn/debian-security buster/updates main contrib non-free
    " > /etc/apt/sources.list
    echo "
    deb http://mirrors.tuna.tsinghua.edu.cn/armbian buster main buster-utils buster-desktop
    " > /etc/apt/sources.list.d/armbian.list
    echo -e "换源成功!"

}





menu(){

    echo "#############################################################"
    echo -e "#                   ${RED}Misaka Linux Toolbox${PLAIN}                    #"
    echo -e "# ${GREEN}作者${PLAIN}: Misaka 的小姐妹                                     #"
    echo -e "# ${GREEN}网址${PLAIN}: https://owo.misaka.rest                             #"
    echo -e "# ${GREEN}论坛${PLAIN}: https://vpsgo.co                                    #"
    echo -e "# ${GREEN}TG群${PLAIN}: https://t.me/misakanetcn                            #"
    echo -e "# ${GREEN}GitHub${PLAIN}: https://github.com/Misaka-blog                    #"
    echo -e "# ${GREEN}Bitbucket${PLAIN}: https://bitbucket.org/misakano7545             #"
    echo -e "# ${GREEN}GitLab${PLAIN}: https://gitlab.com/misaka-blog                    #"
    echo "#############################################################"
    echo ""

    echo -e " ${GREEN}1.${PLAIN} 纯IPv6 VPS设置DNS64服务器"
    echo -e " ${GREEN}2.${PLAIN} 修改登录方式为 root + 密码"
    echo -e " ${GREEN}3.${PLAIN} 开放系统防火墙端口"

    echo -e " ${GREEN}4.${PLAIN} 安装Ddns-go"
    echo -e " ${GREEN}5.${PLAIN} x-ui-msk"
    echo -e " ${GREEN}6.${PLAIN} 安装shellclash"
    echo -e " ${GREEN}7.${PLAIN} 勇哥优选解析"
    echo -e " ${GREEN}8.${PLAIN} 刷新网络"

    # echo -e " ${GREEN}4.${PLAIN} 性能测试"
    # echo -e " ${GREEN}5.${PLAIN} VPS探针"
    # echo -e " ${GREEN}7.${PLAIN} 换源Armbain"
    echo " -------------"
    echo -e " ${GREEN}9.${PLAIN} 更新脚本"
    echo -e " ${GREEN}0.${PLAIN} 退出脚本"

    echo ""
    echo -e "${YELLOW}当前版本${PLAIN}：$version"
    echo -e "${YELLOW}更新日志${PLAIN}：$version_log"
    echo -e "系统 $SYSTEM ${OSID}  ${CMD} 架构：$(archAffix)"
    echo ""

    read -rp " 请输入选项 [0-9]:" menuInput
    case $menuInput in
        1) v6_dns64 ;;
        2) Root_sh ;;
        3) open_ports ;;
        4) ddns_go;;
        5) bash <(wget -qO- https://gitlab.com/yuban/x-ui-msk/-/raw/main/install.sh) ;;
        6) export url='https://fastly.jsdelivr.net/gh/juewuy/ShellClash@master' && wget -q --no-check-certificate -O /tmp/install.sh $url/install.sh  && bash /tmp/install.sh  && source /etc/profile &> /dev/null ;;
        9) wget -N https://gitlab.com/yuban/Rootlinux/raw/main/LinuxTools.sh -O LT.sh && chmod +x LT.sh && bash LT.sh ;;
        7) curl -ksSL https://gitlab.com/rwkgyg/cdnopw/raw/main/cdnopw.sh -o cdnopw.sh && bash cdnopw.sh ;;
        8) curl -ksSL https://gitlab.com/yuban/Rootlinux/-/raw/main/Tool/debain%E5%88%B7%E6%96%B0%E7%BD%91%E7%BB%9C.sh -o ipflash.sh && bash ipflash.sh ;;
        # 1) menu1 ;;
        # 2) menu2 ;;
        # 3) menu3 ;;
        # 4) menu4 ;;
        # 5) menu5 ;;
	# 9) wget -N https://raw.githubusercontent.com/misaka-gh/MisakaLinuxToolbox/master/MisakaToolbox.sh && chmod +x MisakaToolbox.sh && bash MisakaToolbox.sh ;;
        *) clear && exit 1 ;;
    esac
}

clear
menu
