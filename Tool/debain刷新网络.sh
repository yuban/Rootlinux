#!/bin/bash

# 检查当前用户是否为root用户（具有管理员权限）
if [ "$EUID" -ne 0 ]; then
    echo "当前用户没有管理员权限，建议以管理员身份运行脚本。"
    exit 1
fi

echo "正在释放并更新IP地址..."
dhclient -r   # 释放IP地址
dhclient     # 获取新的IP地址

echo "IP地址已成功释放和更新。"
echo

echo "正在清除DNS缓存..."
systemctl restart systemd-resolved.service
echo "DNS缓存已成功清除。"
echo

echo "正在重置网络接口..."
ip link set dev eth0 down
ip link set dev eth0 up

echo "网络故障排除完成。"
